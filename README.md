# Surplus React Native Challenge

Show case your React Native skills!

# Steps and Procedure

1. Download Surplus app in Play Store and/or App Store
2. Check and use the app
3. Develop first similar flows (Splash screen + login/register + random mocked first screen, you can add more, the more merrier) using React Native, use open stock screens examples (or use your owned ones)
4. After developing, when you are submitting notify us by email, please also include in your email your impression and review of our Surplus App (Honest Review will be appreciated)
5. (not mandatory) Give a glimpse of your developed mobile app in a screen recording and short explanations

# App Developemnt Criteria

1. Fork this repository into your public repository (you may use github if you want to, but please notify us)
2. Push, commit many times in the forked repo as you want, but also consider meaningful commit names, time will be counted right after your init commit
3. Add /firmanserdana as contributors
4. Update readme as how to use the code and app (will be checked blindly, so ensure pre/post-steps/configuration will also be provided)
5. Development last step/commit => create a merge request into the main branch, add /firmanserdana as the reviewer 

## Marking and Plus Points
This list is just a guidance, if it's not possible to do the mandatory list, please note it into your readme
1. (Mandatory) Use Open Public API for functional stuff in the random mocked first screen (e.g. weather info at http://www.7timer.info/doc.php?lang=en)
2. (Mandatory) Login credential can be hardcoded, but better to also show case all possible scenarios (success login, failed login, success registering etc.)
3. (Mandatory) App-Styles will be totally considered, make it as beatiful as it can be
4. (Mandaory) Consider multiple phone layouts (the more merrier, but please tell us what phones you are using for developing the app)
5. (Plus Point) ensure cross platform usablity (Android and iOS)
6. (Plus Point) you can use other tech stacks if you want, e.g. Flutter
7. (Plus Point) provide unit test and write the procedures in th readme.

## Tested on:
- device android realme c21y
- emulator android pixel 4 xl
- device ios iphone 13 pro
- emulator ios iphone 13
- emulator ios iphone 8
- emulator ios ipad pro

## How to run code android:
1. go to "AwesomeProject" directory
2. "yarn" on terminal
3. make sure android emulator is active
4. "yarn android" on terminal

## How to build release android apk:
1. go to "AwesomeProject" directory
2. "yarn" on terminal
3. make sure there is "my-upload-key.keystore" inside path "android/app/" if not available, you can contact me to get the keystore or make a new one configuration keystore: https://reactnative.dev/docs/signed-apk-android
4. "yarn build-android" on terminal
5. The generated "app-release.apk" file is in "android/app/build/outputs/apk/release/"

## How to run code ios:
1. go to "AwesomeProject" directory
2. go to "ios" directory
3. "rm Podfile.lock" on terminal
4. "pod install" on terminal
5. go back to "AwesomeProject" directory
6. yarn ios

## How to build release ios ipa:
1. I cannot do this step because i dont have apple developer account membership 😄

## App information:
1. Login.
login emai : admin@gmail.com / Admin@gmail.com
password email : use random password no min/max character. Login not using api, its just mockup with validation check.

2. Fetching api on app. Fetching api is available on "fetch valoran agent" button at home after login. Press the button then it will navigate to another screen and start fetching api, if it success it will show valoran agent, if it fail from server it will show "failed 400" or "failed 500", if it fail because internet connection it will show "failed internet connection".

3. Asyncstorage. This app using asyncstorage library for some purpose like managing data persistance for after login user, so user does no need to login every time open the app.

4. Redux. This app also using redux and redux-saga for state management in app, this library is used for managing state. For some reason app does no need to fetch some of api continuously, app can directly access data from api that already stored on redux.

## How to use app:
I provide the demo video how to use the app to make it more clear and easier to replicate the step to use the app. 

- For ios please kindly check this link: https://drive.google.com/file/d/1Cnz4_AohB5Jesh3QWG5Oa5yehTnKPxWE/view?usp=sharing
- For android please kindly check this link: https://drive.google.com/file/d/1HqKRtCJY6BxlLaZS79F63vaq9vpapeIK/view?usp=sharing

- android apk link: https://drive.google.com/file/d/1N2aPlE00TX8IX0qakB98zLCI0CWh7Eg-/view?usp=sharing

## Thank you
Thank you for giving me a fun, and enjoyable experience challange for making this react-native show case. I hope users are satisfied with the app and please give a feedback ☺️.