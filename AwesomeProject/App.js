// In App.js in a new project

import * as React from 'react';
import { Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { constants } from './src/constants';
import { navigationRef } from './src/configs/RootNavigation';
import Navigation from './src/configs/RootNavigation';

// PAGES
import Initialpage from './src/pages/initialpage';
import Welcomepage from './src/pages/welcomepage';
import Loginpage from './src/pages/loginpage';
import Homepage from './src/pages/homepage';
import Accountpage from './src/pages/accountpage';
import Itempage from './src/pages/itempage';
import { TabLabel } from './src/components/TabLabel/TabLabel';
import { TabIcon } from './src/components/TabIcon/TabIcon';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function InitialLogin() {
  return (
    <Tab.Navigator>
      <Tab.Screen options={{ 
        headerShown: false, 
        tabBarLabel: ({ focused }) => (
          <TabLabel name={'Home'} isactive={focused} />
        ),
        tabBarIcon: ({ focused }) => (
          <TabIcon name={'Home'} isactive={focused} />
        ),
      }} name={constants.pagename.homepage} component={Homepage} />
      <Tab.Screen options={{ 
        headerShown: false, 
        tabBarLabel: ({ focused }) => (
          <TabLabel name={'Account'} isactive={focused} />
        ),
        tabBarIcon: ({ focused }) => (
          <TabIcon name={'Account'} isactive={focused} />
        ),
      }} name={constants.pagename.accountpage} component={Accountpage} />
    </Tab.Navigator>
  );
}

const App = ({}) => {
  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={Navigation.goToInitialRoute}
    >
      <Stack.Navigator initialRouteName={constants.pagename.initialpage}>
        <Stack.Screen 
          options={{headerShown: false}}
          name={constants.pagename.initialpage} 
          component={Initialpage} />
          <Stack.Screen 
          options={{headerShown: false}}
          name={constants.pagename.welcomepage} 
          component={Welcomepage} />
          <Stack.Screen 
          options={{headerShown: false}}
          name={constants.pagename.loginpage} 
          component={Loginpage} />
          <Stack.Screen 
          options={{headerShown: false}}
          name={constants.pagename.initiallogin} 
          component={InitialLogin} />
          <Stack.Screen 
          options={{headerShown: false}}
          name={constants.pagename.itempage} 
          component={Itempage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;