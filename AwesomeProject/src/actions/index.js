import { 
  RESETACCOUNTDATA,
  ACCOUNTDATA,
  GETFROMAPI,
  GETFROMAPI_SUCCESS,
  GETFROMAPI_FAILED,
} from "./types"

export const accountdata = (value) => {
  return {
    type: ACCOUNTDATA,
    payload: value
  }
}

export const resetaccountdata = () => {
  return {
    type: RESETACCOUNTDATA
  }
}

export const getfromapi = () => {
  return {
    type: GETFROMAPI
  }
}

export const getfromapi_success = (value) => {
  return {
    type: GETFROMAPI_SUCCESS,
    payload: value
  }
}

export const getfromapi_failed = (value) => {
  return {
    type: GETFROMAPI_FAILED,
    payload: value
  }
}