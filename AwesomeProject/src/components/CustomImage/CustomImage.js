import * as React from 'react';
import { Button, TextInput, View, Text, Image, ActivityIndicator } from "react-native"
import { Colors } from '../../themes/colors';
import { Logo1 } from '../../assets/index';

const CustomImage = ({uri,style}) => {
  const [isloading, setisloading] = React.useState(false)
  const [iserror, setiserror] = React.useState(false)
  const [issuccess, setissuccess] = React.useState(false)

  return(
    <React.Fragment>
      <View style={{
        position: 'absolute',
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
        justifyContent: 'center', 
        alignItems: 'center'
      }}>
        <ActivityIndicator 
          color={Colors.orange}
          size={"small"} />
      </View>
      {!iserror && (
        <Image
        onError={() => {
          if (uri.includes("http") === true) {
            setisloading(false)
            setiserror(true)
          }
        }}
        style={[{backgroundColor: Colors.white},style]}
        source={{uri: uri}}
      />
      )}
      {iserror && (
        <Logo1 
          width={style.width !== undefined && style.width}
          height={style.height !== undefined && style.height}
        />
      )}
    </React.Fragment>
  )
}

export {
  CustomImage
}