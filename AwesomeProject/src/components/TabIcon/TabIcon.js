import * as React from 'react';
import { Button, TextInput, View, Text } from "react-native"
import { Account, AccountActive, Home, HomeActive } from '../../assets';
import { Colors } from '../../themes/colors';

const TabIcon = ({name,isactive}) => {
  return(
    <React.Fragment>
      {name === 'Account' && (
        isactive ? <AccountActive width={20} height={20}/> : <Account width={20} height={20}/>
      )}
      {name === 'Home' && (
        isactive ? <HomeActive width={20} height={20}/> : <Home width={20} height={20}/>
      )}
    </React.Fragment>
  )
}

export {
  TabIcon
}