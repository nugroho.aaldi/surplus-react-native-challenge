import * as React from 'react';
import { Button, TextInput, View, Text, TouchableOpacity } from "react-native"
import { Account, AccountActive, Backbtn, Home, HomeActive } from '../../assets';
import { Colors } from '../../themes/colors';

const Header = ({title,backbtn}) => {
  return(
    <View style={{
      height: 60,
    }}>
      <View style={{
          width: 60,
          height: 60,
          alignItems: 'center',
          justifyContent: 'center',
          zIndex: 1
        }}>
          <TouchableOpacity onPress={() => {
            if (backbtn != undefined) backbtn()
          }}>
            <Backbtn />
          </TouchableOpacity>
      </View>
      <View style={{
        position: 'absolute',
        top: 0, 
        left: 0, 
        right: 0, 
        bottom: 0, 
        justifyContent: 'center', 
        alignItems: 'center'
      }}>
        <Text style={{
          fontWeight: 'bold',
          fontSize: 16
        }}>
          {title === undefined ? "no name" : title}
        </Text>
      </View>
    </View>
  )
}

export {
  Header
}