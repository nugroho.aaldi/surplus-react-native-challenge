import * as React from 'react';
import { Button, TextInput, View, Text, ActivityIndicator } from "react-native"
import { Colors } from '../../themes/colors';

const LoadingBar = (
  <View style={{
    position: 'absolute',
    top: 0, 
    left: 0, 
    right: 0, 
    bottom: 0, 
    justifyContent: 'center', 
    alignItems: 'center',
    zIndex: 1,
    backgroundColor: Colors.gray,
    opacity: 0.6
  }}>
    <ActivityIndicator 
      color={Colors.orange}
      size={"large"} />
  </View>
)

export {
  LoadingBar
}