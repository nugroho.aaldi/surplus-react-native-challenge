import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import { Button, View, Text } from "react-native"
import { Colors } from '../../themes/colors';

const CustomButton = ({title,onPress,color,disable}) => {
  return(
    (disable === undefined || (disable !== undefined && disable === false)) ? (
      <View style={{
        padding: 5,
        width: '100%'
      }}>
        <TouchableOpacity
          onPress={onPress === undefined ? null : (onPress)}
          style={{
            backgroundColor: color === undefined ? Colors.orange : color,
            borderRadius: 6
          }}
        >
          <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18, fontWeight: '600', padding: 10 }}>
            {title === undefined ? 'BUTTON' : title}
          </Text>
        </TouchableOpacity>
      </View>
    ) : (
      disable !== undefined && disable === true ? (
        <View style={{
          padding: 5,
          width: '100%'
        }}>
          <View
            style={{
              backgroundColor: Colors.gray,
              borderRadius: 6
            }}
          >
            <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 18, fontWeight: '600', padding: 10 }}>
              {title === undefined ? 'BUTTON' : title}
            </Text>
          </View>
        </View>
      ) : (
        null
      )
    )
  )
}

export {
  CustomButton
}