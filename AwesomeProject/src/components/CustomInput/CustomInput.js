import * as React from 'react';
import { Button, TextInput, View, Text } from "react-native"
import { Colors } from '../../themes/colors';

const CustomInput = ({placeholder,error,secureTextEntry,onChangeText,value,onblur}) => {
  return(
    <View>
      <TextInput
        onChangeText={onChangeText}
        value={value}
        onBlur={onblur}
        secureTextEntry={secureTextEntry === undefined ? false : secureTextEntry}
        style={{
          // borderRadius: 6,
          borderBottomColor: Colors.gray,
          borderBottomWidth: 1,
          borderStyle: 'solid',
          padding: 10
        }}
        placeholder={placeholder === undefined ? "placeholder" : placeholder}
      />
      <Text style={{
        fontSize: 12,
        color: Colors.red
      }}>{error}</Text>
    </View>
  )
}

export {
  CustomInput
}