import * as React from 'react';
import { Button, TextInput, View, Text } from "react-native"
import { Colors } from '../../themes/colors';

const TabLabel = ({name,isactive}) => {
  return(
    isactive ? (
      <Text style={{
        color: '#0095C5',
        fontSize: 10
      }}>{name}</Text>
    ) :(
      <Text style={{
        color: '#212121',
        fontSize: 10
      }}>{name}</Text>
    )
  )
}

export {
  TabLabel
}