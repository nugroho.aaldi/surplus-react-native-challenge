import { BackHandler } from "react-native";

const backAction = () => {
  BackHandler.exitApp()
  return true;
};

export {
  backAction
}