// REDUX SAGA
import { call, put, takeLatest } from "redux-saga/effects";

// REDUX
import {
  getfromapi_success,
  getfromapi_failed,
} from "../actions/index";

import {
  GETFROMAPI
} from "../actions/types";
import { reqdatahomepage } from "../pages/homepage/model";

// API
import api from '../services/apiv1';
import { status2xx, status4xx, status5xx } from "../utils";

// Worker Saga: will be fired on ... actions
function* workerTest(api) {
  try {
    // do api call
    const response = yield call(api.testapi)
    console.log('workerTest',response);
    if (response.status === 0) throw response

    if (status2xx(response.status)) {
      let newdata = []
      response.data.forEach(element => {
        if (element.isPlayableCharacter === true && element.fullPortrait !== null) {
          newdata.push(new reqdatahomepage(
            element.uuid,
            element.displayName,
            element.fullPortrait,
            element.description
          ))
        }
      });
      const newparam = {
        data: newdata,
        status: response.status
      }
      console.log('workerTest - newparam',newparam);
      yield put(getfromapi_success(newparam))
    }

    if (status4xx(response.status)) yield put(getfromapi_failed(response))
    if (status5xx(response.status)) yield put(getfromapi_failed(response))
  } catch (e) {
    yield put(getfromapi_failed({ data: [], status: 0 }))
  }
}

export const watcherTest = [
  takeLatest(GETFROMAPI, workerTest, api)
]