import * as React from 'react';
import { View, Text } from "react-native"
import { Imglogin, Logo1 } from "../../assets"
import { spacing } from '../../themes/helper';
import Navigation from '../../configs/RootNavigation';
import { constants } from '../../constants';
import { Colors } from '../../themes/colors';
import { CustomInput } from '../../components/CustomInput/CustomInput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Formik } from 'formik';
import * as yup from 'yup';
import Storage from '../../configs/RootAsyncstorage';
import { SafeAreaView } from 'react-native';
import { CustomButton } from '../../components/CustomButton/CustomButton';
import { LoadingBar } from '../../components/LoadingBar/LoadingBar';

const validationSchema = yup.object().shape({
  email: yup
      .string()
      .required('Email harus dimasukkan.')
      .email('Email format salah.'),
  password: yup
      .string()
      .required('Password harus dimasukkan.')
})

const holderimage = () => {
  return(
    <View>
      <Imglogin />
    </View>
  )
}

const holderbutton = (params,isdisable) => {
  return(
    <SafeAreaView>
      <CustomButton
        title={'LOGIN'}
        onPress={params}
        disable={!(isdisable)}
      />
    </SafeAreaView>
  )
}

const holdertitle = () => {
  return(
    <View style={{
      alignItems: 'center'
    }}>
      <Text style={{
        fontSize: 16,
        fontWeight: 'bold',
        color: Colors.black,
        textAlign: 'center'
      }}>
        Ayo daftar sekarang juga dan{"\n"}dapatkan penawaran terbaik
      </Text>
    </View>
  )
}

const body = (accountdata_d) => {
  const [loading,setloading] = React.useState(false)
  const [err,seterr] = React.useState(false)

  let initialvalue = {
    email: '',
    password: ''
  }

  const goLogin = (params) => {
    console.log(params);
    setloading(true)
    if (params.email.toLowerCase().includes('admin')) {
      accountdata_d(params)
      Storage.storeData('login-cred',params)
      .then(() => {
        Navigation.reset(constants.pagename.initiallogin)
      })
      .catch((e) => console.log(e))
    } else {
      setTimeout(() => {
        setloading(false)
        seterr(true)
      },2000)
    }
  }

  return(
    <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }}>
      {loading === true && LoadingBar}
      <React.Fragment>
        {holderimage()}
        <Formik
          initialValues={
            initialvalue
          }
          validateOnBlur={true}
          validateOnMount={true}
          validationSchema={validationSchema}
          onSubmit={(values) => goLogin(values)}
        >
          {formikProps => (
            <View style={{
              backgroundColor: Colors.white,
              marginTop: -40,
              borderTopEndRadius: 12,
              borderTopStartRadius: 12,
              flex: 1,
              padding: 20
            }}>
              {console.log(formikProps)}
              <View style={{
                flex: 1
              }}>
                {holdertitle()}
                {spacing(20)}
                <CustomInput 
                  value={formikProps.values.email}
                  onChangeText={formikProps.handleChange('email')}
                  onblur={formikProps.handleBlur("email")}
                  placeholder={'Email'}
                  error={formikProps.touched.email ? formikProps.errors.email : null}
                />
                {spacing(5)}
                <CustomInput 
                  value={formikProps.values.password}
                  onChangeText={formikProps.handleChange('password')}
                  onblur={formikProps.handleBlur("password")}
                  secureTextEntry={true}
                  placeholder={'Password'}
                  error={formikProps.touched.password ? formikProps.errors.password : null}
                />
                {err && (
                  <View style={{
                    alignItems: 'center'
                  }}>
                    {spacing(10)}
                    <Text style={{ color: Colors.red }}>Email atau Password tidak cocok.</Text>
                  </View>
                )}
              </View>
              {holderbutton(formikProps.handleSubmit,formikProps.isValid)}
            </View>
          )}
        </Formik>
      </React.Fragment>
    </KeyboardAwareScrollView>
  )
}

export {
  body
}