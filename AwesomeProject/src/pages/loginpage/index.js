import * as React from 'react';
import { Alert, BackHandler, SafeAreaView, ScrollView, Text, View } from "react-native"
import { backAction } from '../../commons/event';
import styles from "./styles";
import { Imglogin } from '../../assets';
import { Colors } from '../../themes/colors';
import { spacing } from '../../themes/helper';
import { body } from './components';
import { connect } from "react-redux";
import { accountdata } from '../../actions/index';
import { LoadingBar } from '../../components/LoadingBar/LoadingBar';
import Navigation from '../../configs/RootNavigation';

const Index = ({
  accountdata_d
}) => {

  return(
    <View style={styles.container}>
      {body(accountdata_d)}
    </View>
  )
}

const mapStateToProps = (state) => ({
  // stringapi_s: state.test.testdata
});

const mapDispatchToProps = (dispatch) => ({
  accountdata_d: (value) => dispatch(accountdata(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(Index)