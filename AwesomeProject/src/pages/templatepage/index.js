import * as React from 'react';
import { SafeAreaView, Text } from "react-native"
import styles from "./styles";
import { connect } from "react-redux";

const Index = ({}) => {

  return(
    <SafeAreaView style={styles.container}>
      <Text>welcome</Text>
    </SafeAreaView>
  )
}

const mapStateToProps = (state) => ({
  // stringapi_s: state.test.testdata
});

const mapDispatchToProps = (dispatch) => ({
  // getfromapi_d: () => dispatch(getfromapi())
})

export default connect(mapStateToProps, mapDispatchToProps)(Index)