import * as React from 'react';
import { SafeAreaView, Text, View, FlatList, Image, ScrollView, Dimensions, TouchableOpacity } from "react-native"
import styles from "./styles";
import { getfromapi } from '../../actions/index';
import { connect } from "react-redux";
import { getappwidth, status2xx, status4xx, status5xx, substr, windowgetwidth } from '../../utils';
import { Colors } from '../../themes/colors';
import imgbanner from '../../assets/images/imgbanner.png';
import promogojek from '../../assets/images/promogojek.jpeg';
import { spacing } from '../../themes/helper';
import Navigation from '../../configs/RootNavigation';
import { constants } from '../../constants';
import { CustomImage } from '../../components/CustomImage/CustomImage';
const DEFAULT_IMAGE_BANNER = Image.resolveAssetSource(imgbanner).uri;
const DEFAULT_IMAGE_PROMOGOJEK = Image.resolveAssetSource(promogojek).uri;
let deviceWidth = Dimensions.get('window').width

const samplecard = (
  <View style={{
    width: deviceWidth,
    height: 340,
    borderRadius: 8,
    overflow: 'hidden',
    padding: 10,
  }}>
    <View style={{
      flex: 1,
      borderRadius: 8,
      overflow: 'hidden'
    }}>
      <View style={{
        flex: 1,
        overflow: 'hidden'
      }}>
        <CustomImage 
          style={{
            width: '100%',
            height: '100%',
          }}
          uri={DEFAULT_IMAGE_PROMOGOJEK}
        />
      </View>
      <View style={{
        padding: 10,
        backgroundColor: 'yellow'
      }}>
        <Text style={{
          fontSize: 16,
          fontWeight: 'bold',
          color: Colors.black
        }}>💰 BONUS 40RB untukmu!</Text>
        {spacing(3)}
        <Text style={{
          color: Colors.black
        }}>Ayo, pakai GoPay Jago & nikmati bonus 40RB GoPay Coins! Cek selengkapnya 👈</Text>
      </View>
    </View>
  </View>
)

const holderbanner = (
  <View style={{
    width: '100%',
    height: 200
  }}>
    <CustomImage 
      style={{
        width: '100%',
        height: '100%'
      }}
      uri={DEFAULT_IMAGE_BANNER}
    />
  </View>
)

const holderallitem = () => {

  const navigatetoitem = () => {
    Navigation.navigate(constants.pagename.itempage)
  }

  return(
    <View style={{
      padding: 10
    }}>
      <Text style={{
        fontSize: 16,
        fontWeight: 'bold',
        color: Colors.black
      }}>All Item</Text>
      {spacing(5)}
      <TouchableOpacity onPress={() => navigatetoitem()}>
        <View style={{
          backgroundColor: '#D2E5F4',
          padding: 10,
          borderRadius: 8
        }}>
          <Text style={{
            color: Colors.black
          }}>fetch valoran agent</Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}

const holderbonus = (
  <React.Fragment>
    <Text style={{
      fontSize: 16,
      fontWeight: 'bold',
      color: Colors.black,
      paddingLeft: 10,
      paddingRight: 10
    }}>Bonus</Text>
    <ScrollView 
      horizontal={true}
      decelerationRate={0}
      snapToInterval={deviceWidth}
      snapToAlignment={"center"}
    >
      {[1,2,3].map((i,x) => (
        <React.Fragment key={x}>
          {samplecard}
        </React.Fragment>
      ))}
    </ScrollView>
  </React.Fragment>
)

const holdercardbody = (
  <View style={{
    backgroundColor: Colors.white,
    height: '100%',
    marginTop: -30,
    borderTopEndRadius: 12,
    borderTopStartRadius: 12,
    overflow: 'hidden',
    paddingTop: 20,
  }}>
    {holderallitem()}
    {spacing(5)}
    {holderbonus}
  </View>
)

const body = () => {
  return(
    <React.Fragment>
      {holderbanner}
      {holdercardbody}
    </React.Fragment>
  )
}

export {
  samplecard,
  body
}