class reqdatahomepage {
  constructor(
    uuid,
    displayname,
    image,
    description
  ){
    this.uuid = uuid;
    this.displayname = displayname;
    this.image = image;
    this.description= description;
  }
}

export {
  reqdatahomepage
}