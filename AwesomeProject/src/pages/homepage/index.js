import * as React from 'react';
import { SafeAreaView, Text, View, FlatList, Image, ScrollView, Dimensions, TouchableOpacity } from "react-native"
import styles from "./styles";
import { getfromapi } from '../../actions/index';
import { connect } from "react-redux";
import { getappwidth, status2xx, status4xx, status5xx, substr, windowgetwidth } from '../../utils';
import { Colors } from '../../themes/colors';
import imgbanner from '../../assets/images/imgbanner.png';
import promogojek from '../../assets/images/promogojek.jpeg';
import { spacing } from '../../themes/helper';
import Navigation from '../../configs/RootNavigation';
import { constants } from '../../constants';
import { CustomImage } from '../../components/CustomImage/CustomImage';
import { body, samplecard } from './components';
const DEFAULT_IMAGE_BANNER = Image.resolveAssetSource(imgbanner).uri;
const DEFAULT_IMAGE_PROMOGOJEK = Image.resolveAssetSource(promogojek).uri;
let deviceWidth = Dimensions.get('window').width

const Index = ({
}) => {
  
  return(
    <ScrollView contentContainerStyle={{ flexGrow: 1 }} style={{
      display: 'flex'
    }}>
      {body()}
    </ScrollView>
  )
}

const mapStateToProps = (state) => ({
  stringapi_s: state.test.testdata
});

const mapDispatchToProps = (dispatch) => ({
  getfromapi_d: () => dispatch(getfromapi())
})

export default connect(mapStateToProps, mapDispatchToProps)(Index)