import * as React from 'react';
import { FlatList, SafeAreaView, Text, View } from "react-native"
import styles from './styles';
import { connect } from "react-redux";
import { getfromapi } from '../../actions/index';
import { status2xx, status4xx, status5xx, substr } from '../../utils';
import { body, holdererror, holderloading, renderItem } from './components';
import { Colors } from '../../themes/colors';
import { CustomImage } from '../../components/CustomImage/CustomImage';
import { Header } from '../../components/Header/Header';
import Navigation from '../../configs/RootNavigation';

const Index = ({
  getfromapi_d,
  stringapi_s
}) => {
  const [data1, setdata1] = React.useState([])
  const [errormsg, seterrormsg] = React.useState(null)

  React.useEffect(() => {
    initiapi()
  },[])

  React.useEffect(() => {
    if (stringapi_s === null) {
      console.log('stringapi_s loading...');
    } else {
      console.log('stringapi_s',stringapi_s);
      if (status2xx(stringapi_s.status)) {setdata1(stringapi_s.data); seterrormsg(null);}
      if (status4xx(stringapi_s.status)) seterrormsg("failed 400")
      if (status5xx(stringapi_s.status)) seterrormsg("failed 500")
      if (stringapi_s.status === 0) seterrormsg("failed internet connection")
    }
  },[stringapi_s])

  const initiapi = () => {
    getfromapi_d()
  }

  return(
    <SafeAreaView style={styles.container}>
      <Header title={'ITEM LIST'} backbtn={Navigation.goBack} />
      {stringapi_s === null ? (
        holderloading()
      ) : (
        <React.Fragment>
          {errormsg !== null && (
            holdererror(errormsg)
          )}
        
          {errormsg === null && (
            body(data1)
          )}
        </React.Fragment>
      )}
      
    </SafeAreaView>
  )
}

const mapStateToProps = (state) => ({
  stringapi_s: state.test.testdata
});

const mapDispatchToProps = (dispatch) => ({
  getfromapi_d: () => dispatch(getfromapi())
})

export default connect(mapStateToProps, mapDispatchToProps)(Index)