import * as React from 'react';
import { SafeAreaView, Text, View, FlatList, Image } from "react-native"
import { CustomImage } from '../../components/CustomImage/CustomImage';
import { Colors } from '../../themes/colors';
import { substr } from '../../utils';
import imgbanner from '../../assets/images/imgbanner.png';
import promogojek from '../../assets/images/promogojek.jpeg';
import { spacing } from '../../themes/helper';
const DEFAULT_IMAGE_BANNER = Image.resolveAssetSource(imgbanner).uri;
const DEFAULT_IMAGE_PROMOGOJEK = Image.resolveAssetSource(promogojek).uri;

const holderloading = () => {
  return(
    <View style={{
      backgroundColor: Colors.orange
    }}>
      <Text style={{
        color: Colors.black,
        textAlign: 'center',
        padding: 5
      }}>Loading...</Text>
    </View>
  )
}

const holdererror = (params) => {
  return(
    <View style={{
      backgroundColor: Colors.red
    }}>
      <Text style={{
        color: Colors.black,
        textAlign: 'center',
        padding: 5
      }}>{params}</Text>
    </View>
  )
}

const renderItem = ({ item }) => (
  <View style={{
    display: 'flex',
    flexDirection: 'row',
    height: 80,
    padding: 5,
    margin: 5,
    borderRadius: 8,
    borderColor: Colors.black,
    borderStyle: 'solid',
    borderWidth: 1
  }}>
    <View style={{
      width: 80
    }}>
      <CustomImage style={{width: '100%', height: '100%'}} uri={item.image} />
    </View>
    <View style={{
      flex: 1
    }}>
      <Text style={{
        fontWeight: 'bold',
        color: Colors.black,
        alignItems: 'center'
      }}>{item.displayname}</Text>
      <Text>
        {substr(80,item.description)}
      </Text>
    </View>
  </View>
);

const body = (params) => {
  return(
    <FlatList
      data={params}
      ItemSeparatorComponent={
        () => <View style={{ padding: 5 }}></View>
      }
      renderItem={renderItem}
      keyExtractor={item => item.uuid}
    />
  )
}

export {
  holderloading,
  holdererror,
  renderItem,
  body
}