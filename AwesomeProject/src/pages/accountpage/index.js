import * as React from 'react';
import { View } from 'react-native';
import { SafeAreaView, Text } from "react-native"
import styles from "./styles";
import Storage from '../../configs/RootAsyncstorage';
import Navigation from '../../configs/RootNavigation';
import { constants } from '../../constants';
import { connect } from "react-redux";
import { CustomButton } from '../../components/CustomButton/CustomButton';
import { Colors } from '../../themes/colors';
import { spacing } from '../../themes/helper';
import { body } from './components';
import { resetaccountdata } from '../../actions/index';

const Index = ({accountdata_s}) => {
  return(
    <SafeAreaView style={styles.container}>
      {body('Email',accountdata_s?.email,resetaccountdata)}
    </SafeAreaView>
  )
}

const mapStateToProps = (state) => ({
  accountdata_s: state.test.accountdata
});

const mapDispatchToProps = (dispatch) => ({
  resetaccountdata_d: () => dispatch(resetaccountdata())
})

export default connect(mapStateToProps, mapDispatchToProps)(Index)