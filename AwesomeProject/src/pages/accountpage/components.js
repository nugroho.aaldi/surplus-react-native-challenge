import * as React from 'react';
import { View } from 'react-native';
import { SafeAreaView, Text } from "react-native"
import styles from "./styles";
import Storage from '../../configs/RootAsyncstorage';
import Navigation from '../../configs/RootNavigation';
import { constants } from '../../constants';
import { connect } from "react-redux";
import { CustomButton } from '../../components/CustomButton/CustomButton';
import { Colors } from '../../themes/colors';
import { spacing } from '../../themes/helper';

const holderforreviewer = () => {
  return(
    <View style={{
      padding: 10,
      alignItems: 'center'
    }}>
      <Text style={{
        color: Colors.gray
      }}>
        Thank you for reviewing the app and code
      </Text>
    </View>
  )
}

const holderbutton = (resetaccountdata) => {

  const logoutaccout = () => {
    Storage.clearAllData()
    .then(() => {
      resetaccountdata()
      Navigation.reset(constants.pagename.welcomepage)
    }).catch((e) => console.log(e))
  }

  return(
    <View style={{
      position: 'absolute',
      width: '100%',
      bottom: 0
    }}>
      <CustomButton
      title={"LOGOUT"}
      onPress={() => {
        logoutaccout()
      }}
      />
    </View>
  )
}

const holderfielddesc = (title,email) => {
  return(
    <View style={{
      borderBottomColor: Colors.gray,
      borderBottomWidth: 0.5,
      borderStyle: 'solid'
    }}>
      <View style={{
        padding: 10
      }}>
        <Text style={{
          color: Colors.gray,
          fontWeight: 'bold',
          fontSize: 16
        }}>{title}</Text>
        {spacing(5)}
        <Text style={{
          color: Colors.black
        }}>{email}</Text>
        {spacing(5)}
      </View>
    </View>
  )
}

const body = (title,email,resetaccountdata) => {
  return(
    <View style={{
      flex: 1
    }}>
      {holderfielddesc(title,email)}
      {holderforreviewer()}
      {holderbutton(resetaccountdata)}
    </View>
  )
}

export {
  body
}