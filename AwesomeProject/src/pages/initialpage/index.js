import * as React from 'react';
import { SafeAreaView } from "react-native"
import * as RootNavigation from '../../configs/RootNavigation';
import { constants } from '../../constants';
import { body } from './components';
import styles from "./styles";
import Navigation from '../../configs/RootNavigation';
import Storage from '../../configs/RootAsyncstorage';
import { connect } from "react-redux";
import { accountdata } from '../../actions/index';

const Index = ({
  accountdata_d
}) => {

  React.useEffect(() => {
    checkuserlogin()
  },[])

  const checkuserlogin = () => {
    Storage.getData('login-cred')
    .then((val) => {
      if (val === undefined || val === null) Navigation.reset(constants.pagename.welcomepage)
      else {accountdata_d(val); Navigation.reset(constants.pagename.initiallogin);}
    })
    .catch((e) => {
      console.log(e)
    })
  }

  return(
    <SafeAreaView style={styles.container}>
      {body()}
    </SafeAreaView>
  )
}

const mapStateToProps = (state) => ({
  // stringapi_s: state.test.testdata
});

const mapDispatchToProps = (dispatch) => ({
  accountdata_d: (value) => dispatch(accountdata(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(Index)