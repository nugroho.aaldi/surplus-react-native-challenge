import { StyleSheet } from "react-native";
import mainstyles from '../../themes/mainstyles';

const styles = StyleSheet.create({
  ...mainstyles,
  body: {
    flex: 1,
    justifyContent: "center"
  }
});

export default styles