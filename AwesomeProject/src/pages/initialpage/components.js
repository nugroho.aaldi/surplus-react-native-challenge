import * as React from 'react';
import { ActivityIndicator, View } from "react-native"
import { Colors } from '../../themes/colors';
import styles from "./styles";

const body = () => {
  return(
    <View style={styles.body}>
      <ActivityIndicator 
        color={Colors.orange}
        size={"large"} />
    </View>
  )
}

export {
  body
}