import * as React from 'react';
import { Alert, BackHandler, SafeAreaView, Text, View } from "react-native"
import { backAction } from '../../commons/event';
import { body, holderdeskripsi, holderlogo } from './components';
import styles from "./styles";
import { connect } from "react-redux";

const Index = ({}) => {

  React.useEffect(() => {
    // const backHandler = BackHandler.addEventListener(
    //   "hardwareBackPress",
    //   backAction
    // );

    // return () => backHandler.remove();
  },[])

  return(
    <SafeAreaView style={styles.container}>
      {body()}
    </SafeAreaView>
  )
}

const mapStateToProps = (state) => ({
  // stringapi_s: state.test.testdata
});

const mapDispatchToProps = (dispatch) => ({
  // getfromapi_d: () => dispatch(getfromapi())
})

export default connect(mapStateToProps, mapDispatchToProps)(Index)