import * as React from 'react';
import { View, Text } from "react-native"
import { Logo1 } from "../../assets"
import { CustomButton } from '../../components/CustomButton/CustomButton';
import { spacing } from '../../themes/helper';
import Navigation from '../../configs/RootNavigation';
import { constants } from '../../constants';
import { Colors } from '../../themes/colors';

const holderlogo = () => {
  return(
    <View style={{
      alignItems: 'center'
    }}>
      <Logo1 />
    </View>
  )
}

const holderdeskripsi = () => {
  return(
    <View style={{
      alignItems: 'center'
    }}>
      <Text style={{
        color: 'black',
        fontSize: 16,
        fontWeight: 'bold'
      }}>Welcome and enjoy 😊</Text>
    </View>
  )
}

const holderbutton = () => {

  const GoToLogin = () => {
    Navigation.navigate(constants.pagename.loginpage)
  }

  return(
    <View style={{
      position: 'absolute',
      bottom: 0,
      width: '100%',
      paddingBottom:10
    }}>
      <CustomButton
        onPress={() => GoToLogin()}
        title={"LOGIN"}
      />
    </View>
  )
}

const body = () => {

  return(
    <View style={{
      flex: 1,
      justifyContent: 'center',
      backgroundColor: Colors.white
    }}>
      {holderlogo()}
      {spacing(10)}
      {holderdeskripsi()}
      {holderbutton()}
    </View>
  )
}

export {
  holderlogo,
  holderdeskripsi,
  body
}