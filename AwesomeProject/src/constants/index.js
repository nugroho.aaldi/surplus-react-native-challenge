const constants = {
  pagename: {
    initiallogin: "Initiallogin",
    initialpage: "Initialpage",
    welcomepage: "Welcomepage",
    loginpage: "Loginpage",
    homepage: "Homepage",
    accountpage: "Accountpage",
    itempage: "Itempage"
  },
  status: {
    '2xx': [
      200,
      201,
      202,
      203,
      204
    ],
    '4xx': [
      400,
      401,
      402,
      403,
      404
    ],
    '5xx': [
      500,
      501,
      502,
      503,
      504
    ]
  }
}

export {
  constants
}