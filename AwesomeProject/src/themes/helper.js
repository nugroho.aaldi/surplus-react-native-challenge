import * as React from 'react';
import { View } from "react-native"

const spacing = (size) => {
  return(
    <View style={{
      padding: size
    }}></View>
  )
}

export {
  spacing
}