const Colors = {
  orange: 'orange',
  white: 'white',
  gray: 'gray',
  red: 'red',
  purewhite: '#FFFFFF',
  black: 'black'
}

export {
  Colors
}