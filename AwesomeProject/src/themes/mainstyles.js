import { StyleSheet } from "react-native";
import { Colors } from "./colors";

const mainstyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  }
});

export default mainstyles