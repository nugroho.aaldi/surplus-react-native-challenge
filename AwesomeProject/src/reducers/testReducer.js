import { 
  RESETACCOUNTDATA,
  ACCOUNTDATA,
  GETFROMAPI,
  GETFROMAPI_SUCCESS,
  GETFROMAPI_FAILED
} from "../actions/types"

const initialState = {
  testdata: null,
  accountdata: null
}

export default function testReducers(state = initialState, action) {
  switch (action.type) {
    case RESETACCOUNTDATA:
      return {  
        ...state, accountdata: null
      }
    case ACCOUNTDATA:
      return {  
        ...state, accountdata: action.payload
      }
    case GETFROMAPI:
      return {  
        ...state, testdata: null
      }
    case GETFROMAPI_SUCCESS:
      return {  
        ...state, testdata: action.payload
      }
    case GETFROMAPI_FAILED:
      return {  
        ...state, testdata: action.payload
      }
    default:
      return state
  }
}