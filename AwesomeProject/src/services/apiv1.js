import apisauce from 'apisauce';
import { checkobjectorno, status2xx, status4xx, status5xx } from '../utils';

const create = (baseURL = '') => {
  const api = apisauce.create({
    baseURL
  });

  let setheaders = { 
    'Content-Type': 'application/json'
  }

  const post_methode = 'POST';
  const get_methode = 'GET';
  const delete_methode = 'DELETE';
  const patch_methode = 'PATCH';
  const CORS = 'cors';

  const responseparser = (val1,val2) => {
    let template = { data: null, status: 0 }
    try {

      console.log('responseparser - val1',val1);
      console.log('responseparser - val2',val2);

      template.data = JSON.stringify(val1)
      if (val2 === 'failed') return template

      template.status = val2
      if (status2xx(val2)) {
        template.data = checkobjectorno(val1)
      } else {
        template.data = []
      }
      return template
    } catch (error) {
      console.log('responseparser',error);
      return template.data = []
    }
  }

  const testapi = async (data) => {
    const URL = 'https://valorant-api.com/v1/agents';
    const header = {
      ...setheaders
    }

    const config = {
      method: get_methode,
      headers: header,
      timeoutInterval: 30000
    }
    console.log('config-headers',config);
    try {
      const resp = await fetch(URL,config)
      const data = await resp.json()
      const status = resp.status
      if (status2xx(status)) {
        // 200
        return responseparser(data.data,status)
      } else if (status4xx(status)) {
        // 400
        return responseparser(data,status)
      } else if (status5xx(status)) {
        // 500
        return responseparser(data,status)
      } else {
        throw 'error status resp'
      }
    } catch (err) {
      return responseparser(err,'failed')
    }
}

    return {
      api,
      testapi
    };
};

export default create();