import Logo1 from "./images/logo1.svg";
import Imglogin from "./images/imglogin.svg";
import Account from "./images/Account.svg";
import Home from "./images/Home.svg";
import AccountActive from "./images/AccountActive.svg";
import HomeActive from "./images/HomeActive.svg";
import Backbtn from "./images/backbtn.svg";


export {
  Logo1,
  Imglogin,
  Account,
  Home,
  AccountActive,
  HomeActive,
  Backbtn
}