import { constants } from "../constants";
import React, { useState, useEffect } from "react";
import { View, StyleSheet, Text, Dimensions } from "react-native";

const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

export const checkobjectorno = (value) => {
  if (value != null || value != undefined) {
    if (typeof value === 'string') {
      return JSON.parse(value)
    } else {
      return value
    }
  }
};

export const status2xx = (value) => {
  try {
    const x = constants.status['2xx'].filter(x => x == value)
    if (x.length === 0) {
      return false
    } else {
      return true
    }
  } catch (error) {
    return false
  }
};

export const status4xx = (value) => {
  try {
    const x = constants.status['4xx'].filter(x => x == value)
    if (x.length === 0) {
      return false
    } else {
      return true
    }
  } catch (error) {
    return false
  }
};

export const status5xx = (value) => {
  try {
    const x = constants.status['5xx'].filter(x => x == value)
    if (x.length === 0) {
      return false
    } else {
      return true
    }
  } catch (error) {
    return false
  }
};

export const substr = (max,stringtocut) => {
  if (stringtocut.length < max) {
    return stringtocut
  } else {
    return stringtocut.substring(0,max) + '...'
  }
}

export const windowgetwidth = Dimensions.get("screen").width

export const getappwidth = () => {
  const [dimensions, setDimensions] = useState({ window, screen });

  useEffect(() => {
    const subscription = Dimensions.addEventListener(
      "change",
      ({ window, screen }) => {
        setDimensions({ window, screen });
      }
    );
    return () => subscription?.remove();
  });

  return (
    <View>
      <Text style={styles.header}>Window Dimensions</Text>
      {Object.entries(dimensions.window).map(([key, value]) => (
        <Text>{key} - {value}</Text>
      ))}
      <Text style={styles.header}>Screen Dimensions</Text>
      {Object.entries(dimensions.screen).map(([key, value]) => (
        <Text>{key} - {value}</Text>
      ))}
    </View>
  );
}