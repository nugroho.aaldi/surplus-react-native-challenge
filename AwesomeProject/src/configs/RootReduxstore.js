import { applyMiddleware, legacy_createStore as createStore } from 'redux';
import createSagaMiddleware from 'redux-saga'

import allReducers from '../reducers';
import reduxSaga from "./RootReduxsaga";

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()

// mount it on the Store
const store = createStore(
  allReducers,
  applyMiddleware(sagaMiddleware)
)

// then run the saga
sagaMiddleware.run(reduxSaga)

// // without redux saga
// const store = createStore(allReducers)

export default store