import { all } from 'redux-saga/effects' 

import { watcherTest } from '../sagas/testSaga'

export default function* reduxSaga() {
    yield all([
        ...watcherTest
    ])
}