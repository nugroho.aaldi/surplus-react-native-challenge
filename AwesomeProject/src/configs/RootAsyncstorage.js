import AsyncStorage from '@react-native-async-storage/async-storage';

const storeData = async (keyname,value) => {
  try {
    const jsonValue = JSON.stringify(value)
    await AsyncStorage.setItem(keyname, jsonValue)
  } catch (e) {
    // saving error
  }
}

const getData = async (keyname) => {
  try {
    const jsonValue = await AsyncStorage.getItem(keyname)
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch(e) {
    // error reading value
  }
}

const clearAllData = async () => {
  try {
    await AsyncStorage.clear()
  } catch(e) {
    // error reading value
  }
}

export default {
  storeData,
  getData,
  clearAllData
}