import { CommonActions, StackActions, createNavigationContainerRef } from '@react-navigation/native';

export const navigationRef = createNavigationContainerRef();
let initialRoute = null;

const navigate = (route) => {
  console.log(`trying to navigate to ${route}`);
  if (navigationRef.isReady()) {
    console.log(`navigationRef ready, navigating to ${route}`);
    // Some business logic, but basically just this
    const someAction = CommonActions.navigate(route)
    navigationRef.current.dispatch(someAction);
  } else {
    // Navigation container not yet loaded, set initial route.
    console.log(`navigation not ready, setting initialRoute: ${route}`);
    initialRoute = route;
  }
}

const goBack = () => {
  if (navigationRef.isReady()) {
    const someAction = CommonActions.goBack()
    navigationRef.current.dispatch(someAction);
  }
}

const reset = (route) => {
  console.log(`trying to reset to ${route}`);
  if (navigationRef.isReady()) {
    console.log(`navigationRef ready, reseting to ${route}`);
    // Some business logic, but basically just this
    const someAction = CommonActions.reset({
      index: 0,
      routes: [{
        name: route
      }]
    })
    navigationRef.current.dispatch(someAction);
  } else {
    // Navigation container not yet loaded, set initial route.
    console.log(`navigation not ready, setting initialRoute: ${route}`);
    initialRoute = route;
  }
}

const goToInitialRoute = () => {
  console.log(`initial route: ${initialRoute}`);
  if (initialRoute) {
    console.log('going to initial route');
    navigate(initialRoute);
  }
}

export default {
  navigate,
  goToInitialRoute,
  reset,
  goBack
};