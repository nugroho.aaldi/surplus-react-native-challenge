/**
 * @format
 */

 import * as React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

// REDUX
import { Provider } from 'react-redux';
import store from "./src/configs/RootReduxstore";

const RootApp = () => {
  return(
    <Provider store={store}>
      <App />
    </Provider>
  )
}

AppRegistry.registerComponent(appName, () => RootApp);
